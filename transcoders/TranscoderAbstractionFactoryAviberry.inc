<?php
/**
 * @file
 * File containing class TranscoderAbstractionFactoryAviberry
 *
 * Copyright 2012 Movavi (email : support@movavi.com)
 */

/**
 * Class that handles Aviberry transcoding.
 */
class TranscoderAbstractionFactoryAviberry extends TranscoderAbstractionFactory implements TranscoderFactoryInterface {

  protected $options = array();

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Set input file.
   *
   * @param array $file
   *   file
   */
  public function setInput(array $file) {
    parent::setInput($file);
    $this->options['input'] = file_create_url($this->settings['input']['uri']);
  }

  /**
   * Aviberry doesn't support this method.
   *
   * @return array
   *   codecs
   */
  public function getCodecs() {
    $auto = t('Default for this extension');

    $codecs = array(
      'encode' => array(
        'video' => array(
          '' => $auto,
          'h264' => 'H.264',
          'vp8' => 'VP8',
          'theora' => 'Theora',
          'vp6' => 'VP6',
          'mpeg4' => 'MPEG-4',
          'wmv' => 'WMV',
        ),
        'audio' => array(
          '' => $auto,
          'aac' => 'AAC',
          'mp3' => 'MP3',
          'vorbis' => 'Vorbis',
          'wma' => 'WMA',
        ),
      ),
      'decode' => array(),
    );
    return $codecs;
  }

  /**
   * Aviberry doesn't support this method.
   *
   * @return array
   *   null
   */
  public function getPixelFormats() {
    return array();
  }

  /**
   * Set options.
   *
   * @param array $options
   *   options
   *
   * @return boolean
   *   success
   */
  public function setOptions(array $options) {
    parent::setOptions($options);

    foreach ($options as $key => $value) {
      if (empty($value) || $value === 'none') {
        continue;
      }

      switch ($key) {
        case 'video_extension':
          $this->options['output']['format'] = $value;
          break;

        case 'video_preset':
        case 'default':
          break;

        default:
          $this->options['output'][$key] = $value;
      }
    }

    // thumbnails.
    $this->options['output']['thumbnails'] = array(
      'format' => $this->options['output']['thumbnails']['format'],
      'number' => intval(variable_get('video_thumbnail_count', 5)),
      'thumb_size' => variable_get('video_thumbnail_size', '320x240'),
      'prefix' => 'thumbnail-' . $this->settings['input']['fid'],
    );

    return TRUE;
  }

  /**
   * Set output file.
   *
   * @param mixed $output_directory
   *   output_directory
   * @param mixed $output_name
   *   output_name
   * @param mixed $overwrite_mode
   *   overwrite_mode
   */
  public function setOutput($output_directory, $output_name, $overwrite_mode = FILE_EXISTS_REPLACE) {
    parent::setOutput($output_directory, $output_name, $overwrite_mode);
    $this->options['output']['label'] = 'video-' . $this->settings['input']['fid'];
    $this->options['output']['filename'] = $this->settings['filename'];
    $this->options['output']['public'] = 1;
  }

  /**
   * Extract frames.
   *
   * @param mixed $destination_scheme
   *   destination_scheme
   * @param mixed $format
   *   format
   *
   * @return boolean
   *   success
   *
   * @see TranscoderFactoryInterface.extractFrames()
   */
  public function extractFrames($destination_scheme, $format) {

    // For new videos, this function is never called, because all thumbnails are
    // extracted and saved to the databases during the post back handler in
    // TranscoderAbstractionFactoryAviberry::processPostback().
    return FALSE;
  }

  /**
   * Start conversion.
   *
   * @return boolean|stdClass
   *   boolean|stdClass
   * @throws Exception
   */
  public function execute() {
    try {

      $target_url = str_replace('public://', variable_get('video_aviberry_ftp_connect_string', NULL) . '/', $this->settings['base_url']) . '/' . $this->settings['filename'];
      if (variable_get('video_aviberry_ftp_user_name') && variable_get('video_aviberry_ftp_user_password')) {
        $target_url = str_replace('ftp://', 'ftp://' . variable_get('video_aviberry_ftp_user_name') . ':' . variable_get('video_aviberry_ftp_user_password') . '@', $target_url);
      }

      if (!isset($this->options['output']['video_aviberry_preset'])) {
        throw new Exception(sprintf('Run-time error. Choose a preset.'));
      }

      $preset['preset_id'] = $this->options['output']['video_aviberry_preset'];
      $preset['preset_data'] = '{"GetMediaInfo":"COMPLETE"}';

      $result = AviberryAPI::startConversion($this->options['input'], $target_url, $preset);

      if ($result == FALSE) {
        throw new Exception(sprintf('Error startConversion. Return from %s: Error: %s, Message: %s', AviberryAPI::getServer(), xmlrpc_errno(), xmlrpc_error_msg()));
      }
      if (!strlen($result['conversion_id'])) {
        throw new Exception(sprintf('Error startConversion. "conversion_id" is empty'));
      }

      $output_uri = $this->settings['base_url'] . '/' . $this->settings['filename'];

      $output = new stdClass();
      $output->filename = $this->settings['filename'];
      $output->uri = $output_uri;
      $output->filemime = file_get_mimetype($output_uri);
      $output->filesize = 0;
      $output->timestamp = time();
      $output->jobid = NULL;
      $output->duration = 0;

      $aviberry_record = array(
        'vid' => $this->settings['input']['vid'],
        'fid' => $this->settings['input']['fid'],
        'sid' => $result['conversion_id'],
      );
      drupal_write_record('aviberry', $aviberry_record);

      return $output;
    } catch (Exception $e) {
      $errors[] = $e->getMessage();
      watchdog(
          'aviberry',
          'Aviberry reports errors while startConversion %file:<br/>!errorlist',
          array(
            '%file' => $this->settings['filename'],
            '!errorlist' => theme(
                'item_list',
                array('items' => $errors)
            ),
          ),
          WATCHDOG_ERROR
      );
      return FALSE;
    }
  }

  /**
   * Get name.
   *
   * @return string
   *   name
   */
  public function getName() {
    return 'Aviberry';
  }

  /**
   * Get value.
   *
   * @return string
   *   value
   */
  public function getValue() {
    return 'TranscoderAbstractionFactoryAviberry';
  }

  /**
   * Is available.
   *
   * @param mixed $errormsg
   *   errormsg
   *
   * @return boolean
   *   success
   */
  public function isAvailable(&$errormsg) {
    registry_rebuild();
    return TRUE;
  }

  /**
   * Get version.
   *
   * @return string
   *   version
   */
  public function getVersion() {
    return '1.0';
  }

  /**
   * Admin form.
   *
   * @return array
   *   form
   */
  public function adminSettings() {
    $form = array();

    $form['aviberry_info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Aviberry'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#states' => array(
        'visible' => array(
          ':input[name=video_convertor]' => array('value' => 'TranscoderAbstractionFactoryAviberry'),
        ),
      ),
    );
    $form['aviberry_info']['video_aviberry_api_server'] = array(
      '#type' => 'textfield',
      '#title' => t('Aviberry API host name'),
      '#default_value' => variable_get('video_aviberry_api_server', 'www.aviberry.com'),
      '#description' => t('Aviberry API host name. For example www.aviberry.com'),
    );
    $form['aviberry_info']['video_aviberry_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Aviberry API key'),
      '#default_value' => variable_get('video_aviberry_api_key', NULL),
      '#description' => t('Aviberry API Key. Get an Aviberry account at www.aviberry.com'),
    );
    $form['aviberry_info']['video_aviberry_api_pass'] = array(
      '#type' => 'textfield',
      '#title' => t('Aviberry API pass'),
      '#default_value' => variable_get('video_aviberry_api_pass', NULL),
      '#description' => t('Aviberry API pass.'),
    );

    $ftp_value = variable_get('video_aviberry_ftp_connect_string', 'ftp://' . $_SERVER['SERVER_NAME'] . '/');

    $form['aviberry_info']['video_aviberry_ftp_connect_string'] = array(
      '#type' => 'textfield',
      '#title' => t('FTP connect string where the converted files will be saved'),
      '#default_value' => $ftp_value,
      '#description' => t('Configure FTP acces to Public file system path. Default &quot;@path&quot;.', array('@path' => $_SERVER['SERVER_NAME'] . '/sites/default/files')),
    );
    $form['aviberry_info']['video_aviberry_ftp_user_name'] = array(
      '#type' => 'textfield',
      '#title' => t('FTP user name'),
      '#default_value' => variable_get('video_aviberry_ftp_user_name', NULL),
      '#description' => '',
    );
    $form['aviberry_info']['video_aviberry_ftp_user_password'] = array(
      '#type' => 'password',
      '#title' => t('FTP user password'),
      '#default_value' => variable_get('video_aviberry_ftp_user_password', NULL),
      '#description' => '',
    );
    $form['aviberry_info']['video_aviberry_thumbnail_size'] = array(
      '#type' => 'select',
      '#title' => t('Dimension of thumbnails'),
      '#default_value' => variable_get('video_aviberry_thumbnail_size', '320x240'),
      '#options' => video_utility::getDimensions(),
    );

    return $form;
  }

  /**
   * Validate admin form.
   *
   * @param array $form
   *   form
   * @param array $form_state
   *   form_state
   */
  public function adminSettingsValidate($form, &$form_state) {
    $values = $form_state['values'];
    $rpc_result = AviberryAPI::checkAuth($values['video_aviberry_api_server'], $values['video_aviberry_api_key'], $values['video_aviberry_api_pass']);
    if ($rpc_result === FALSE) {
      form_error(
        $form['aviberry_info']['video_aviberry_api_key'],
        t(
            'Error return from @server: Error: @errno, Message: @message',
            array(
              '@server' => AviberryAPI::getServer(),
              '@errno' => xmlrpc_errno(), '@message' => xmlrpc_error_msg(),
            )
        )
      );
    }

    $temp_file = $values['video_aviberry_ftp_connect_string'] . '/videos/converted/test.file';

    if ($values['video_aviberry_ftp_user_name'] && $values['video_aviberry_ftp_user_password']) {
      $temp_file = str_replace('ftp://', 'ftp://' . $values['video_aviberry_ftp_user_name'] . ':' . $values['video_aviberry_ftp_user_password'] . '@', $temp_file);
    }

    $error_message = '';
    if ($handle = fopen($temp_file, 'ab')) {
      if (!fwrite($handle, 'test')) {
        $error_message = t('File write error "@file"', array('@file' => $temp_file));
      }
      else {
        @unlink($temp_file);
      }
      fclose($handle);
    }
    else {
      $error_message = t('File write error. Could not open the source file "@file"', array('@file' => $temp_file));
    }

    if ($error_message) {
      form_error(
        $form['aviberry_info']['video_aviberry_ftp_connect_string'],
        t('Error: @error', array('@error' => $error_message))
      );
    }
    variable_set('video_use_preset_wxh', 0);
  }

  /**
   * Get available output file formats from the transcoder.
   *
   * @param boolean $type
   *   type
   *
   * @return array
   *   formats
   */
  public function getAvailableFormats($type = FALSE) {
    return array(
      'mp4' => 'MP4',
      'flv' => 'FLV',
      'webm' => 'WebM',
      'mov' => 'MOV',
      'avi' => 'AVI',
      '3gpp' => '3GPP',
      '3gpp2' => '3GPP2',
      'mpg' => 'MPG',
      'wmv' => 'WMV',
      'mp3' => 'MP3',
    );
  }

}
