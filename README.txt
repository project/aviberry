REQUIREMENTS
------------
- Video module (http://drupal.org/project/video) 7.x-2.6, 7.x-2.7 tested
- Aviberry account (www.aviberry.com)
- Drupal 7.14 tested, all works fine


DESCRIPTION
------------
Aviberry Drupal Video Conversion Module extends Drupal Video module,
offering an enhanced video converter in addition to the default engine.
The module ensures professional video quality and stable encoding to formats
perfect for a chosen media player. Learn more on supported formats at
http://www.aviberry.com/supported_formats.html.


INSTALLATION AND CONFIGURATION
------------
1. Download the latest version 
2. Unpack the module to your Drupal site folder (\sites\all\modules)
3. Configure the Video module to use Aviberry Drupal Video Conversion Module
(Transcoders -> Video Transcoder -> Aviberry)
4. Adjust Aviberry Transcoder settings
5. In the Video module options configure preset settings
6. To speed up conversion, activate 'Video convert on node submit'
(Video plugin Options -> General)
7. Keep on using the Aviberry module.
 
CHANGELOG
------------
* 1.0
- New release! 


MAINTAINERS
-----------
www.movavi.com - share@movavi.com
