<?php
/**
 * @file
 * Aviberry API class
 *
 * Copyright 2012 Movavi (email : support@movavi.com)
 */

/**
 * Class for working with the Aviberry API
 */
class AviberryAPI {

  protected static $version = 'v1.1.1';
  protected static $schema = 'http';
  protected static $protocol = 'xml';
  protected static $server = 'www.aviberry.com';
  protected static $path = 'api/';
  protected static $apiKey;
  protected static $apiPass;
  protected static $methodsAPI = array(
    'getConversionCount' => 'getConversionCount',
    'startConversion' => 'startConversion',
    'cancelConversion' => 'cancelConversion',
  );

  /**
   * Get URL for API access.
   *
   * @return string
   *   URL
   */
  protected static function getURL() {
    return self::$schema . '://' . self::$apiKey . ':' . self::$apiPass . '@' . self::$server . '/' . self::$path . self::$version . '/' . self::$protocol . '/';
  }

  /**
   * Get server name.
   *
   * @return string
   *   server name
   */
  public static function getServer() {
    return self::$server;
  }

  /**
   * Set credential parameters from drupal variables.
   */
  protected static function getCredentialFromVariable() {
    self::$apiKey = variable_get('video_aviberry_api_key', NULL);
    self::$apiPass = variable_get('video_aviberry_api_pass', NULL);
    self::$server = variable_get('video_aviberry_api_server', NULL);
  }

  /**
   * Set credential parameters.
   *
   * @param string $server
   *   server
   * @param string $key
   *   key
   * @param string $pass
   *   pass
   */
  protected static function setCredential($server, $key, $pass) {
    self::$server = $server;
    self::$apiKey = $key;
    self::$apiPass = $pass;
  }

  /**
   * Check the user authorization.
   *
   * @param string $server
   *   server
   * @param string $key
   *   key
   * @param string $pass
   *   pass
   *
   * @return int
   *   conversion count
   */
  public static function checkAuth($server, $key, $pass) {
    self::setCredential($server, $key, $pass);
    return self::getConversionCount();
  }

  /**
   * Call Aviberry API method getConversionCount.
   *
   * @return int
   *   conversion count
   */
  protected static function getConversionCount() {
    $options = array(
      self::$methodsAPI['getConversionCount'] => array(''),
    );
    return xmlrpc(self::getURL(), $options);
  }

  /**
   * Call Aviberry API method cancelConversion.
   *
   * @param string $sid
   *   sid
   *
   * @return xmlrpc
   *   xmlrpc
   */
  public static function cancelConversion($sid) {
    self::getCredentialFromVariable();

    $options = array(
      self::$methodsAPI['cancelConversion'] => array(
        'sid' => $sid,
      ),
    );
    return xmlrpc(self::getURL(), $options);
  }

  /**
   * Call Aviberry API method startConversion.
   *
   * @param string $source_url
   *   source_url
   * @param string $target_url
   *   target_url
   * @param array $preset
   *   preset
   *
   * @return xmlrpc
   *   array
   */
  public static function startConversion($source_url, $target_url, $preset) {
    self::getCredentialFromVariable();

    $preview_array = explode('x', variable_get('video_aviberry_thumbnail_size', '320x240'));

    $options = array(
      self::$methodsAPI['startConversion'] => array(
        'source_url' => $source_url,
        'target_url' => $target_url,
        'preset' => $preset,
        'transform' => '',
        'preview' => array(
          'width' => $preview_array[0],
          'height' => $preview_array[1],
          'resize_method' => 1,
          'resize_quality' => 1,
        ),
        'callback' => array(
          'url' => 'http://' . self::$apiKey . ':' . self::$apiPass . '@' . $_SERVER['HTTP_HOST'] . '/xmlrpc.php',
          'method' => 'aviberry.callback',
          'protocol' => 'xml',
          'associate_with' => 'PARENT',
          'call_on_cancel' => '1',
        ),
        'params' => '',
        'data' => array(
          'api_request_source' => 'drupal',
          'aviberry_version_cms' => VERSION,
          'aviberry_version_plugin' => self::getAviberryModuleVersion(),
        ),
      ),
    );

    return xmlrpc(self::getURL(), $options);
  }

  /**
   * Get viberry module version.
   *
   * @return version
   *   string
   */
  public static function getAviberryModuleVersion() {
    $info = system_get_info('module', 'aviberry');
    return !empty($info['version']) ? $info['version'] : '';
  }
}
